﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class SimpleUdpSrvr
{
    public static void Main()
    {
        // Deklarasi Variabel Username dan Pesan
        string input;
        byte[] data = new byte[1024];
        byte[] data1 = new byte[1024];

        // Listen Client
        IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 8080);
        UdpClient newsock = new UdpClient(ipep);

        // Input Username Server
        Console.Write("Input Your Name : ");
        string username = Console.ReadLine();
        Console.WriteLine("Waiting for a client...");

        // Menerima Username Client
        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        data = newsock.Receive(ref sender);
        string ClientName = Encoding.ASCII.GetString(data, 0, data.Length);

        // Menampilkan Username Client
        Console.WriteLine("Message received from {0}:", sender.ToString());
        Console.WriteLine("Hi, I'm " + Encoding.ASCII.GetString(data, 0, data.Length));
        
        // Mengirim Username Server ke Client
        data = Encoding.ASCII.GetBytes(username);
        newsock.Send(data, data.Length, sender);

        while(true)
        {
            // Menerima Pesan dari Client
            data = newsock.Receive(ref sender);
            // Menampilkan Pesan beserta Username Client
            Console.WriteLine(ClientName + " : " + Encoding.ASCII.GetString(data, 0, data.Length));
            // Input Pesan untuk Dikirimkan ke Client
            Console.Write(username + " : ");
            input = Console.ReadLine();
            // Mengirim Pesan ke Client
            data1 = Encoding.ASCII.GetBytes(input);
            newsock.Send(data1, data1.Length, sender);
        }
    }
}