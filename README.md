# Praktikum Jaringan Komputer UDP Server

421018017_Muhammad Shodiqul Ihsan

This code is for UDP Server.
Description of this code:
I use C# language. This program for a simple chat with client.
For the first, server must input its username. Then, server waits for the client
to bind. After that, server listen to client's IP Address and Port. Than, server
waits for client sending message. After that, server receive a username and
message from client. But, server never know that UDP is still connect or not.
So, when client ended its connection, server still on.

I'm partner up with 4210181028_Dicky Dwi Darmawan